use crate::model::ancestry::{Ancestry, ExplicitAncestry, ExplicitGenotypeAncestry};
use crate::model::description::Description;

pub fn ancestry(anc: &Ancestry, description: &Description) -> ExplicitAncestry {
    anc.into_iter()
        .map(|&pop_hash_of_segment| &description.population(pop_hash_of_segment).code)
        .cloned()
        .collect()
}

pub fn genotype_ancestry(
    chr_ancestries: &(Ancestry, Ancestry),
    description: &Description,
) -> ExplicitGenotypeAncestry {
    ExplicitGenotypeAncestry(
        ancestry(&chr_ancestries.0, &description),
        ancestry(&chr_ancestries.1, &description),
    )
}
