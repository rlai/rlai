use std::cmp::Ordering::Equal;
use std::collections::BTreeMap;

use log::{log, warn};

use crate::model::ancestry::Ancestry;
use crate::model::ancestry::PopulationHash;
use crate::model::description::Description;
use crate::model::distance::{ChromosomeDistanceMap, GenotypeDistanceMap};
use crate::model::genotype::Schema;
use crate::model::haploblock::{Haploblock, HaploblockSequence, WeighingMethod};
use crate::model::segment::SegmentContext;

pub fn find_weak_blocks(segment_indices_by_block: &[Vec<usize>], threshold: usize) {
    let mut nb_weak_blocks = 0;

    for (block_index, segment_indices) in segment_indices_by_block.iter().enumerate() {
        if segment_indices.len() < threshold {
            nb_weak_blocks += 1;
            warn!(
                "Block #{} contains {} segments, which is fewer than {}.",
                block_index,
                segment_indices.len(),
                threshold
            );
        }
    }

    if nb_weak_blocks > 0 {
        warn!("There are {} weak blocks in total.", nb_weak_blocks);
    }
}

pub fn distribute_segment_indices_across_blocks(
    schema: &Schema,
    haploblock_seq: &HaploblockSequence,
    segment_ctx: &SegmentContext,
) -> Vec<Vec<usize>> {
    let HaploblockSequence(block_seq) = haploblock_seq;

    let mut segment_indices_by_block: Vec<Vec<usize>> =
        block_seq.iter().map(|_| Vec::new()).collect();

    let mut block_iter = block_seq.iter().enumerate();
    let (mut block_index, mut block) = block_iter.next().unwrap();

    let nb_markers = schema.len();
    let nb_segments = segment_ctx.number_of_segments(nb_markers);

    'segment_loop: for segment_index in 0..nb_segments {
        let (segment_left_bound, segment_right_bound) =
            segment_ctx.segment_bp_bounds_from_segment_index(schema, segment_index);

        // This segment can't be fully covered by this block,
        // but the next segment can be, so get the next one.
        // If the next one is not covered it's ok,
        // because `continue` will happen before anything else.
        if segment_left_bound < block.left_bound as u32 {
            continue;
        }

        // This segment and all subsequent segments can't be covered by this block,
        // so get the next block.
        // We need to loop through all the blocks that don't cover this segment,
        // before continuing with the next segment.
        while (block.right_bound as u32) < segment_right_bound {
            match block_iter.next() {
                Some((next_block_index, next_block)) => {
                    block = next_block;
                    block_index = next_block_index;
                }
                None => break 'segment_loop,
            }
        }

        // At this point the block contains the segment.

        segment_indices_by_block[block_index].push(segment_index);
    }

    segment_indices_by_block
}

fn harmonic_block_ancestry(
    mean_distance_map: &ChromosomeDistanceMap,
    inner_segment_indices: &[usize],
) -> (Vec<PopulationHash>, f32) {
    let mut weight_sum_by_pop_hash: BTreeMap<PopulationHash, f32> = mean_distance_map
        .iter()
        .next()
        .expect("TODO")
        .keys()
        .map(|&pop_hash| (pop_hash, 0.0))
        .collect();

    for &segment_index in inner_segment_indices {
        let mut segment_mean_distances: Vec<_> = mean_distance_map[segment_index].iter().collect();
        segment_mean_distances.sort_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Equal));

        for (i, (pop_hash, _)) in segment_mean_distances.iter().enumerate() {
            if let Some(weight_sum) = weight_sum_by_pop_hash.get_mut(&pop_hash) {
                *weight_sum += 1.0 / (i + 1) as f32;
            }
        }
    }

    let (_, &max_weight_sum) = weight_sum_by_pop_hash
        .iter()
        .max_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Equal))
        .unwrap();

    let plausible_pop_hashes: Vec<PopulationHash> = weight_sum_by_pop_hash
        .iter()
        .filter(|(_, &weight_sum)| (weight_sum - max_weight_sum).abs() < std::f32::EPSILON)
        .map(|(&pop_hash, _)| pop_hash)
        .collect();

    (plausible_pop_hashes, max_weight_sum)
}

fn mismatch_count_block_ancestry(
    mean_distance_map: &ChromosomeDistanceMap,
    inner_segment_indices: &[usize],
) -> (Vec<PopulationHash>, f32) {
    let mut distance_sum_by_pop_hash: BTreeMap<PopulationHash, f32> = mean_distance_map
        .iter()
        .next()
        .expect("TODO")
        .keys()
        .map(|&pop_hash| (pop_hash, 0.0))
        .collect();

    for &segment_index in inner_segment_indices {
        for (pop_hash, mean_distance) in mean_distance_map[segment_index].iter() {
            if let Some(distance_sum) = distance_sum_by_pop_hash.get_mut(&pop_hash) {
                *distance_sum += mean_distance;
            }
        }
    }

    let (_, &min_distance_sum) = distance_sum_by_pop_hash
        .iter()
        .min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Equal))
        .unwrap();

    let plausible_pop_hashes: Vec<PopulationHash> = distance_sum_by_pop_hash
        .iter()
        .filter(|(_, &distance_sum)| (distance_sum - min_distance_sum).abs() < std::f32::EPSILON)
        .map(|(&pop_hash, _)| pop_hash)
        .collect();

    (plausible_pop_hashes, min_distance_sum)
}

fn block_ancestry(
    block: &Haploblock,
    segment_indices: &[usize],
    mean_distance_map: &ChromosomeDistanceMap,
    weighing_method: WeighingMethod,
    description: &Description,
) -> PopulationHash {
    let (plausible_pop_hashes, _) = match weighing_method {
        WeighingMethod::Harmonic => harmonic_block_ancestry(mean_distance_map, segment_indices),
        WeighingMethod::MismatchCount => {
            mismatch_count_block_ancestry(mean_distance_map, segment_indices)
        }
    };

    let plausible_ancestries: Vec<_> = plausible_pop_hashes
        .iter()
        .map(|&pop_hash| description.population(pop_hash).code.clone())
        .collect();

    if plausible_ancestries.len() > 1 {
        warn!(
            "The ancestries equally plausible for the block ({}, {}): {}.",
            block.left_bound,
            block.right_bound,
            plausible_ancestries.join(", ")
        );
        warn!("{} was arbitrarily picked.", plausible_ancestries[0]);
    }

    plausible_pop_hashes[0]
}

pub fn ancestry(
    haploblock_seq: &HaploblockSequence,
    mean_distance_map: &ChromosomeDistanceMap,
    segment_indices_by_block: &[Vec<usize>],
    weighing_method: WeighingMethod,
    description: &Description,
) -> Ancestry {
    let HaploblockSequence(block_seq) = haploblock_seq;

    block_seq
        .iter()
        .zip(segment_indices_by_block)
        .map(|(block, segment_indices)| {
            block_ancestry(
                block,
                &segment_indices,
                mean_distance_map,
                weighing_method,
                description,
            )
        }).collect()
}

pub fn genotype_ancestry(
    haploblock_seq: &HaploblockSequence,
    gt_mean_distance_map: &GenotypeDistanceMap,
    segment_indices_by_block: &[Vec<usize>],
    weighing_method: WeighingMethod,
    description: &Description,
) -> (Ancestry, Ancestry) {
    (
        ancestry(
            haploblock_seq,
            &gt_mean_distance_map.0,
            segment_indices_by_block,
            weighing_method,
            description,
        ),
        ancestry(
            haploblock_seq,
            &gt_mean_distance_map.1,
            segment_indices_by_block,
            weighing_method,
            description,
        ),
    )
}
