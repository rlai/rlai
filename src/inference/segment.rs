use std::cmp::Ordering::Equal;
use std::collections::BTreeMap;

use crate::model::ancestry::Ancestry;
use crate::model::ancestry::PopulationHash;
use crate::model::distance::{ChromosomeDistanceMap, GenotypeDistanceMap};
use crate::model::segment::SegmentContext;

pub fn ancestry(mean_distance_map: &ChromosomeDistanceMap) -> Ancestry {
    mean_distance_map
        .into_iter()
        .map(|mean_distance_by_pop_hash| {
            let pop_hash_ref = mean_distance_by_pop_hash
                .into_iter()
                .min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Equal))
                .unwrap()
                .0;
            *pop_hash_ref
        }).collect()
}

pub fn genotype_ancestry(gt_mean_distance_map: &GenotypeDistanceMap) -> (Ancestry, Ancestry) {
    (
        ancestry(&gt_mean_distance_map.0),
        ancestry(&gt_mean_distance_map.1),
    )
}

fn cover_left_bound(marker_idx: usize, segment_ctx: &SegmentContext) -> usize {
    let SegmentContext { size, shift } = *segment_ctx;
    let segment_left_bound = marker_idx as i32 - size as i32 + 1;

    // If marker's cover starts with the first segment
    // then `truncated_segment_left_bound` will be `0`.
    let truncated_segment_left_bound = if segment_left_bound > 0 {
        segment_left_bound as usize
    } else {
        0
    };

    if shift == 0 {
        return truncated_segment_left_bound;
    }

    // If marker's cover starts with the first segment
    // then `rem` will be 0.
    let rem = truncated_segment_left_bound % shift;

    // If marker's cover starts with the first segment
    // then 0 will be returned, which is correct.
    if rem == 0 {
        truncated_segment_left_bound
    } else {
        truncated_segment_left_bound + (shift - rem)
    }
}

pub fn fine_ancestry(anc: &Ancestry, segment_ctx: &SegmentContext) -> Ancestry {
    let nb_segments = anc.len();
    let SegmentContext { size, shift } = segment_ctx;

    // All segments but first account for `shift` additional markers.
    let nb_markers = size + (nb_segments - 1) * shift;

    (0..nb_markers)
        .map(|i| {
            let cover_bound = cover_left_bound(i, segment_ctx);
            let cover_size = (i - cover_bound) / shift + 1;
            let segment_idx = cover_bound / shift;

            let mut count: BTreeMap<PopulationHash, usize> = BTreeMap::new();

            for &x in anc[segment_idx..].iter().take(cover_size) {
                *count.entry(x).or_insert(0) += 1;
            }

            *count.iter().max_by_key(|&(_, v)| v).unwrap().0
        }).collect()
}

pub fn fine_genotype_ancestry(
    chr_ancestries: &(Ancestry, Ancestry),
    segment_ctx: &SegmentContext,
) -> (Ancestry, Ancestry) {
    (
        fine_ancestry(&chr_ancestries.0, segment_ctx),
        fine_ancestry(&chr_ancestries.1, segment_ctx),
    )
}
