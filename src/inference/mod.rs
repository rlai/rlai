use std::collections::BTreeMap;

use crate::model::ancestry::PopulationHash;
use crate::model::description::Description;
use crate::model::distance::{
    ChromosomeDistance, ChromosomeDistanceMap, CumulativeDifference, GenotypeDistanceMap,
};
use crate::model::genotype::{Chromosome, Genotype, GenotypeSet};
use crate::model::segment::SegmentContext;

pub mod explicit;
pub mod haploblock;
pub mod segment;

fn compare_chr_with_chr(chr: &Chromosome, other_chr: &Chromosome) -> CumulativeDifference {
    let mut cumulative_difference = Vec::with_capacity(chr.len());
    let mut acc = 0;

    for (chr_value, other_chr_value) in chr.iter().zip(other_chr.iter()) {
        acc += (chr_value != other_chr_value) as usize;
        cumulative_difference.push(acc);
    }

    cumulative_difference
}

pub fn compare_chr_with_gt(
    chr: &Chromosome,
    genotype: &Genotype,
) -> (CumulativeDifference, CumulativeDifference) {
    (
        compare_chr_with_chr(&chr, &genotype.0),
        compare_chr_with_chr(&chr, &genotype.1),
    )
}

fn segment_distance(
    cumulative_difference: &CumulativeDifference,
    left_bound: usize,
    size: usize,
) -> usize {
    if left_bound == 0 {
        cumulative_difference[size - 1]
    } else {
        cumulative_difference[left_bound + size - 1] - cumulative_difference[left_bound - 1]
    }
}

pub fn mean_chr_distance(
    cumulative_differences: &[CumulativeDifference],
    nb_markers: usize,
    segment_ctx: &SegmentContext,
) -> ChromosomeDistance {
    let SegmentContext { size, shift } = *segment_ctx;
    let segment_left_bounds = (0..nb_markers).filter(|&b| b % shift == 0 && b + size <= nb_markers);

    let distance_sums_by_segment = segment_left_bounds.map(|left_bound| {
        cumulative_differences
            .iter()
            .map(|difference| segment_distance(&difference, left_bound, size))
            .sum::<usize>() as f32
    });

    let nb_chromosomes = cumulative_differences.len() as f32;

    distance_sums_by_segment
        .map(|distance_sum| distance_sum / nb_chromosomes)
        .collect()
}

pub fn mean_chr_distance_map(
    chr: &Chromosome,
    reference_set: &GenotypeSet,
    nb_markers: usize,
    segment_ctx: &SegmentContext,
    description: &Description,
) -> ChromosomeDistanceMap {
    let differences_by_sample_id = reference_set
        .iter()
        .map(|(sample_id, genotype)| (sample_id.clone(), compare_chr_with_gt(&chr, genotype)))
        .collect::<BTreeMap<String, (CumulativeDifference, CumulativeDifference)>>();

    let mut differences_by_pop_hash: BTreeMap<PopulationHash, Vec<CumulativeDifference>> =
        BTreeMap::new();

    for (sample_id, difference_pair) in differences_by_sample_id.into_iter() {
        let pop_hash = description.hash(sample_id);

        differences_by_pop_hash
            .entry(pop_hash)
            .or_insert_with(Vec::new)
            .push(difference_pair.0);

        differences_by_pop_hash
            .get_mut(&pop_hash)
            .unwrap()
            .push(difference_pair.1)
    }

    let mean_chr_distance_by_pop_hash = differences_by_pop_hash
        .into_iter()
        .map(|(pop_hash, differences)| {
            (
                pop_hash,
                mean_chr_distance(&differences, nb_markers, segment_ctx),
            )
        }).collect::<BTreeMap<PopulationHash, ChromosomeDistance>>();

    let mut mean_distance_map: ChromosomeDistanceMap = Vec::new();

    for (pop_hash, mean_chr_distance) in mean_chr_distance_by_pop_hash.into_iter() {
        for (idx, &mean_distance) in mean_chr_distance.iter().enumerate() {
            if mean_distance_map.get_mut(idx).is_none() {
                mean_distance_map.push(BTreeMap::new());
            }

            mean_distance_map[idx].insert(pop_hash, mean_distance);
        }
    }

    mean_distance_map
}

pub fn mean_genotype_distance_map(
    genotype: &Genotype,
    reference_set: &GenotypeSet,
    nb_markers: usize,
    segment_ctx: &SegmentContext,
    description: &Description,
) -> GenotypeDistanceMap {
    GenotypeDistanceMap(
        mean_chr_distance_map(
            &genotype.0,
            reference_set,
            nb_markers,
            segment_ctx,
            description,
        ),
        mean_chr_distance_map(
            &genotype.1,
            reference_set,
            nb_markers,
            segment_ctx,
            description,
        ),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compare_chr_with_chr() {
        let chr: Chromosome = vec![1, 0, 1, 0, 1, 1, 0, 0, 1];
        let other_chr: Chromosome = vec![1, 1, 1, 1, 0, 1, 1, 0, 1];

        let cmp = compare_chr_with_chr(&chr, &other_chr);
        let correct_cmp: CumulativeDifference = vec![0, 1, 1, 2, 3, 3, 4, 4, 4];

        assert_eq!(cmp, correct_cmp);
    }

    #[test]
    fn test_compare_chr_with_gt() {
        let chr: Chromosome = vec![1, 1, 1, 1, 0, 1, 1, 0, 1];
        let gt: Genotype = Genotype(
            vec![1, 0, 1, 0, 1, 1, 0, 0, 1],
            vec![1, 0, 1, 0, 1, 1, 0, 0, 0],
        );

        let cum_diff = compare_chr_with_gt(&chr, &gt);
        let correct_cum_diff: (CumulativeDifference, CumulativeDifference) = (
            vec![0, 1, 1, 2, 3, 3, 4, 4, 4],
            vec![0, 1, 1, 2, 3, 3, 4, 4, 5],
        );

        assert_eq!(cum_diff, correct_cum_diff);
    }

    #[test]
    fn test_segment_distance() {
        let cum_diff: CumulativeDifference = vec![0, 1, 1, 2, 3, 3, 4, 4, 4];

        assert_eq!(segment_distance(&cum_diff, 1, 2), 1);
        assert_eq!(segment_distance(&cum_diff, 1, 5), 3);
        assert_eq!(segment_distance(&cum_diff, 3, 2), 2);
    }

    #[test]
    fn test_mean_chr_distance() {
        let nb_markers = 9;

        let chr: Chromosome = vec![1, 0, 1, 0, 1, 1, 0, 0, 1];

        let other_chrs: [Chromosome; 4] = [
            vec![1, 0, 1, 0, 1, 0, 0, 1, 0],
            vec![1, 1, 1, 1, 1, 1, 0, 0, 0],
            vec![1, 0, 1, 0, 1, 0, 1, 0, 1],
            vec![1, 1, 1, 1, 1, 1, 0, 0, 0],
        ];

        let cum_diffs: Vec<CumulativeDifference> = other_chrs
            .into_iter()
            .map(|other_chr| compare_chr_with_chr(&chr, &other_chr))
            .collect();

        let segment_ctx = SegmentContext { size: 3, shift: 2 };
        let mean_dist = mean_chr_distance(&cum_diffs, nb_markers, &segment_ctx);

        assert_eq!(mean_dist.len(), 4);
        assert_eq!(mean_dist[0], 0.5);
        assert_eq!(mean_dist[1], 0.5);
        assert_eq!(mean_dist[2], 0.75);
        assert_eq!(mean_dist[3], 1.25);
    }
}
