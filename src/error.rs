use failure::Fail;
use rust_htslib::bcf as htslib;
use serde_json;

use std::io;
use std::path::PathBuf;

type CsvError = csv::Error;
type JsonError = serde_json::error::Error;

/// An error that can occur during Configuration reading.
#[derive(Debug, Fail)]
pub enum Configuration {
    /// ???
    #[fail(display = "Configuration file failed to open")]
    Path(#[cause] io::Error),
    /// ???
    #[fail(display = "Configuration file couldn't be parsed as JSON")]
    Json(#[cause] JsonError),
    /// ???
    #[fail(
        display = "File specified as '{}' couldn't be parsed as TSV",
        field
    )]
    Tsv {
        field: String,
        #[cause]
        cause: CsvError,
    },
    #[fail(display = "File specified as 'description' contains unknown sample id")]
    UnknownSampleId,
    /// ???
    #[fail(
        display = "File {:?} specified as '{}' in configuration doesn't exist",
        path,
        field
    )]
    FileNotFound { field: String, path: PathBuf },
    /// ???
    #[fail(
        display = "Value specified as '{}' in configuration has to be positive",
        field
    )]
    ZeroValue { field: String },
}

/// An error that can occur during BCF reading.
#[derive(Debug, Fail)]
pub enum Input {
    /// An I/O error that occurred because of invalid path.
    #[fail(display = "Input file failed to open")]
    Path(#[cause] htslib::BCFPathError),
    /// An error that occurred while reading genotypes from a record.
    #[fail(
        display = "Input file contains tags that are missing, undefined or different from definition"
    )]
    Format(#[cause] htslib::record::FormatReadError),
    /// An error that occurred while reading next record from BCF.
    #[fail(display = "Input file contains invalid records")]
    Read(#[cause] htslib::ReadError),
    /// An error that occurred because BCF contains a missing genotype.
    #[fail(
        display = "{} has a missing genotype at {}",
        sample_id,
        marker_pos
    )]
    MissingGenotype { sample_id: String, marker_pos: u32 },
    /// An error that occurred because BCF contains an unphased genotype.
    #[fail(
        display = "{} has unphased genotypes at {}",
        sample_id,
        marker_pos
    )]
    UnphasedGenotype { sample_id: String, marker_pos: u32 },
    /// An error that occurred because input and reference schemas don't match.
    #[fail(display = "Input and reference schemas don't match")]
    SchemaMismatch,
}

/// An error that can occur during dumping.
#[derive(Debug, Fail)]
pub enum Dump {
    #[fail(display = "Dump couldn't be written as TSV")]
    Tsv(#[cause] CsvError),
}

#[derive(Debug, Fail)]
pub enum Execution {
    #[fail(display = "There is a problem with configuration file")]
    Configuration(#[cause] Configuration),
    #[fail(display = "There is a problem with input file")]
    Input(#[cause] Input),
    #[fail(display = "There is a problem with dump file")]
    Dump(#[cause] Dump),
}
