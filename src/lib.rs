#![feature(custom_attribute)]

pub mod error;
pub mod htslib;
pub mod inference;
pub mod model;
pub mod progress;

use log::{debug, log, warn};

use crate::inference::haploblock;
use crate::model::configuration::Configuration;
use crate::model::context;
use crate::model::context::Context;
use crate::model::segment::SegmentContext;
use crate::progress::ProgressBuilder;

pub fn infer_ancestry(
    ctx: &context::Inference,
    configuration: &Configuration,
) -> Result<(), error::Execution> {
    let nb_markers = ctx.reference.schema.len();

    debug!("Schema contains {} markers.", nb_markers);

    let ids = ctx.ids.clone();

    debug!("Dataset contains {} samples.", ids.len());

    let Configuration { size, shift, .. } = *configuration;

    let segment_ctx = SegmentContext { shift, size };

    let nb_uncovered_markers = (nb_markers - size) % shift;

    if nb_uncovered_markers > 0 {
        warn!(
            "Last {} markers cannot be covered by any segment.",
            nb_uncovered_markers
        );
    }

    let segment_indices_by_block = ctx.reference.haploblock_ctx.as_ref().map(|haploblock_ctx| {
        let segment_indices_by_block = haploblock::distribute_segment_indices_across_blocks(
            &ctx.schema,
            &haploblock_ctx.sequence,
            &segment_ctx,
        );

        let threshold = 10;

        haploblock::find_weak_blocks(&segment_indices_by_block, threshold);

        segment_indices_by_block
    });

    let progress_bar = ProgressBuilder::new()
        .start_message("Inferring ancestry...")
        .finish_message("Finished ancestry inference.")
        .build();

    let ids = ctx.ids.clone();

    progress_bar
        .traverse(ids.into_iter(), |input_id| {
            let input = &ctx.genotype_set[&input_id];

            let mean_distance_distribution = inference::mean_genotype_distance_map(
                &input,
                &ctx.reference.genotype_set,
                nb_markers,
                &segment_ctx,
                &ctx.reference.description,
            );

            let ancestry = if let Some(ref haploblock_ctx) = ctx.reference.haploblock_ctx {
                inference::haploblock::genotype_ancestry(
                    &haploblock_ctx.sequence,
                    &mean_distance_distribution,
                    segment_indices_by_block.as_ref().unwrap(),
                    haploblock_ctx.weighing_method,
                    &ctx.reference.description,
                )
            } else {
                let segment_ancestry =
                    inference::segment::genotype_ancestry(&mean_distance_distribution);

                inference::segment::fine_genotype_ancestry(&segment_ancestry, &segment_ctx)
            };

            let explicit_ancestry =
                inference::explicit::genotype_ancestry(&ancestry, &ctx.reference.description);

            explicit_ancestry
                .dump(&input_id, &configuration.out_dir)
                .map_err(error::Execution::Dump)
        }).collect::<Result<Vec<()>, error::Execution>>()?;

    Ok(())
}

pub fn cross_validate(
    mut ctx: context::Reference,
    configuration: &Configuration,
) -> Result<(), error::Execution> {
    let nb_markers = ctx.schema.len();

    debug!("Schema contains {} markers.", nb_markers);

    let ids = ctx.ids.clone();

    debug!("Dataset contains {} samples.", ids.len());

    let Configuration { size, shift, .. } = *configuration;

    let segment_ctx = SegmentContext { shift, size };

    let nb_uncovered_markers = (nb_markers - size) % shift;

    if nb_uncovered_markers > 0 {
        warn!(
            "Last {} markers cannot be covered by any segment.",
            nb_uncovered_markers
        );
    }

    let segment_indices_by_block = ctx.haploblock_ctx.as_ref().map(|haploblock_ctx| {
        let segment_indices_by_block = haploblock::distribute_segment_indices_across_blocks(
            &ctx.schema,
            &haploblock_ctx.sequence,
            &segment_ctx,
        );

        let threshold = 10;

        haploblock::find_weak_blocks(&segment_indices_by_block, threshold);

        segment_indices_by_block
    });

    let progress_bar = ProgressBuilder::new()
        .start_message("Performing cross-validation...")
        .format_message("Processing {}...")
        .finish_message("Finished cross-validation.")
        .build();

    progress_bar
        .traverse(ids.into_iter(), |sample_id| {
            let input = ctx.genotype_set.remove(&sample_id).unwrap();

            let mean_distance_distribution = inference::mean_genotype_distance_map(
                &input,
                &ctx.genotype_set,
                nb_markers,
                &segment_ctx,
                &ctx.description,
            );

            if let Some(dump_verbosity) = configuration.dump_verbosity {
                mean_distance_distribution
                    .dump(
                        &sample_id,
                        dump_verbosity,
                        &ctx.description,
                        &configuration.out_dir,
                    ).map_err(error::Execution::Dump)?;
            }

            let ancestry = if let Some(ref haploblock_ctx) = ctx.haploblock_ctx {
                inference::haploblock::genotype_ancestry(
                    &haploblock_ctx.sequence,
                    &mean_distance_distribution,
                    segment_indices_by_block.as_ref().unwrap(),
                    haploblock_ctx.weighing_method,
                    &ctx.description,
                )
            } else {
                let segment_ancestry =
                    inference::segment::genotype_ancestry(&mean_distance_distribution);

                inference::segment::fine_genotype_ancestry(&segment_ancestry, &segment_ctx)
            };

            ctx.genotype_set.insert(sample_id.clone(), input);

            let explicit_ancestry =
                inference::explicit::genotype_ancestry(&ancestry, &ctx.description);

            explicit_ancestry
                .dump(&sample_id, &configuration.out_dir)
                .map_err(error::Execution::Dump)
        }).collect::<Result<Vec<()>, error::Execution>>()?;

    Ok(())
}

/// Creates a context appropriate for the tasks specified in the configuration.
///
/// Context bundles together all the data that is needed for the task (inference or cross-validation).
///
/// # Errors
/// This function will return an error if any step of the pipeline fails.
pub fn dispatch(configuration: &Configuration) -> Result<(), error::Execution> {
    match Context::new(&configuration)? {
        Context::Inference(ctx) => {
            debug!("Dispatched to ancestry inference.");
            infer_ancestry(&ctx, &configuration)?
        }
        Context::Reference(ctx) => {
            debug!("Dispatched to cross-validation.");
            cross_validate(ctx, &configuration)?
        }
    };

    Ok(())
}
