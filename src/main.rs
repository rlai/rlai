use std::fs;
use std::path::PathBuf;

use env_logger::Builder;
use failure::Fail;
use log::{debug, error, info, log, LevelFilter};
use structopt::StructOpt;

use rlai::error;
use rlai::model::configuration::Configuration;

#[derive(StructOpt, Debug)]
struct Opt {
    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,

    /// BCF file to process
    #[structopt(name = "CONFIGURATION", parse(from_os_str))]
    configuration: PathBuf,
}

/// The top level function that reads a configuration file and executes tasks specified in it.
///
/// `execute` tries to read configuration file from disk, validates if it is valid and dispatches on it.
///
/// # Errors
/// This function will return an error if any step of the pipeline fails.
fn execute(opt: Opt) -> Result<(), error::Execution> {
    let raw_configuration =
        Configuration::from_path(opt.configuration).map_err(error::Execution::Configuration)?;

    info!("Read specified configuration file.");

    let configuration = raw_configuration
        .normalize()
        .map_err(error::Execution::Configuration)?;

    debug!("Transformed configuration into its normal form.");

    configuration
        .validate()
        .map_err(error::Execution::Configuration)?;

    info!("Made sure configuration is valid.");

    fs::DirBuilder::new()
        .recursive(true)
        .create(&configuration.out_dir)
        .unwrap();

    debug!("Made sure 'out_dir' exists.");

    // Proceed with the configuration.

    rlai::dispatch(&configuration)?;

    Ok(())
}

fn main() {
    let opt = Opt::from_args();

    // This specifies logging level according to the arguments.

    let level_filter = match opt.verbose {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    Builder::new().filter(None, level_filter).init();

    debug!("Initialized logging.");

    // We have to handle everything returned by `execute` in `main`.

    match execute(opt) {
        Ok(_) => (),
        Err(err) => {
            for cause in err.causes() {
                error!("{}.", cause);
            }
        }
    }
}
