use crate::model::genotype::Schema;

pub struct SegmentContext {
    pub shift: usize,
    pub size: usize,
}

impl SegmentContext {
    pub fn segment_bp_bounds_from_segment_index(
        &self,
        schema: &Schema,
        segment_index: usize,
    ) -> (u32, u32) {
        let left_bound_index = self.shift * segment_index;

        let left_bound = schema[left_bound_index];

        let right_bound_index = left_bound_index + self.size;

        let right_bound = if right_bound_index < schema.len() {
            schema[right_bound_index]
        } else {
            schema[right_bound_index - 1]
        };

        (left_bound, right_bound)
    }

    pub fn number_of_segments(&self, nb_markers: usize) -> usize {
        (nb_markers - self.size) / self.shift
    }
}
