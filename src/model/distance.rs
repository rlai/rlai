extern crate csv;

use std::cmp::Ordering::Equal;
use std::collections::BTreeMap;
use std::path::Path;

use crate::error::Dump as DumpError;
use crate::model::ancestry::PopulationHash;

pub type CumulativeDifference = Vec<usize>;

pub type ChromosomeDistance = Vec<f32>;

pub type DistanceMap = BTreeMap<PopulationHash, f32>;

pub type ChromosomeDistanceMap = Vec<DistanceMap>;

pub struct GenotypeDistanceMap(pub ChromosomeDistanceMap, pub ChromosomeDistanceMap);

use crate::model::description::Description;

impl GenotypeDistanceMap {
    pub fn dump(
        &self,
        sample_id: &str,
        verbosity: usize,
        description: &Description,
        out_dir: &Path,
    ) -> Result<(), DumpError> {
        let path = out_dir.join(sample_id).with_extension("dist");

        let mut wtr = csv::WriterBuilder::new()
            .has_headers(true)
            .delimiter(b'\t')
            .from_path(path)
            .map_err(DumpError::Tsv)?;

        wtr.serialize((
            "chr_index",
            "segment_index",
            "known_origin_code",
            "origin_code",
            "mean_distance",
        )).map_err(DumpError::Tsv)?;

        let known_origin_code = &description.population(description.hash(sample_id)).code;

        let chr_pair = [&self.0, &self.1];

        for (chr_index, chr) in chr_pair.iter().enumerate() {
            for (segment_index, distance_map) in chr.iter().enumerate() {
                let mut distance_pairs: Vec<_> = distance_map.iter().collect();

                distance_pairs.sort_unstable_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Equal));

                for (&pop_hash_of_segment, mean_distance) in distance_pairs.iter().take(verbosity) {
                    let origin_code = &description.population(pop_hash_of_segment).code;

                    wtr.serialize((
                        chr_index,
                        segment_index,
                        known_origin_code,
                        origin_code,
                        mean_distance,
                    )).map_err(DumpError::Tsv)?;
                }
            }
        }

        Ok(())
    }
}
