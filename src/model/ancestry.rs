extern crate csv;

use std::collections::BTreeMap;
use std::path::Path;

use crate::error::Dump as DumpError;

#[derive(Hash, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Population {
    pub id: String,
    pub code: String,
}

pub type PopulationHash = u64;

pub type Ancestry = Vec<PopulationHash>;

pub type ExplicitAncestry = Vec<String>;

pub struct ExplicitGenotypeAncestry(pub ExplicitAncestry, pub ExplicitAncestry);

pub type AncestrySet = BTreeMap<String, ExplicitGenotypeAncestry>;

impl ExplicitGenotypeAncestry {
    pub fn dump(&self, sample_id: &str, out_dir: &Path) -> Result<(), DumpError> {
        let path = out_dir.join(sample_id).with_extension("tsv");

        let mut wtr = csv::WriterBuilder::new()
            .has_headers(false)
            .delimiter(b'\t')
            .from_path(path)
            .map_err(DumpError::Tsv)?;

        for (left_code, right_code) in self.0.iter().zip(&self.1) {
            wtr.serialize((left_code, right_code))
                .map_err(DumpError::Tsv)?;
        }

        Ok(())
    }
}
