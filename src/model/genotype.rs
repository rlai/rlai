use std::collections::BTreeMap;

pub type Schema = Vec<u32>;

pub type AlleleIndex = u8;

pub type Chromosome = Vec<AlleleIndex>;

#[derive(Debug, PartialEq)]
pub struct Genotype(pub Chromosome, pub Chromosome);

pub type GenotypeSet = BTreeMap<String, Genotype>;
