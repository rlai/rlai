use std::env;
use std::fs;
use std::path::{Path, PathBuf};

use serde;
use serde_derive::{Deserialize, Serialize};
use serde_json;
use serde_json::error::Error as JsonError;

use crate::error::Configuration as ConfigurationError;
use crate::model::haploblock::WeighingMethod;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct HaploblockConfiguration {
    pub sequence: PathBuf,
    pub weighing_method: WeighingMethod,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Configuration {
    pub input: Option<PathBuf>,
    pub reference: PathBuf,
    pub description: PathBuf,
    pub shift: usize,
    pub size: usize,
    pub out_dir: PathBuf,
    pub haploblock: Option<HaploblockConfiguration>,
    pub dump_verbosity: Option<usize>,
}

fn read_json<T>(file: fs::File) -> Result<T, JsonError>
where
    for<'de> T: serde::Deserialize<'de>,
{
    let json: T = serde_json::from_reader(file)?;
    Ok(json)
}

impl Configuration {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Configuration, ConfigurationError> {
        let mb_file = env::current_dir()
            .map(|dir| dir.join(path))
            .and_then(fs::File::open)
            .map_err(ConfigurationError::Path);

        match mb_file {
            Ok(file) => read_json(file).map_err(ConfigurationError::Json),
            Err(e) => Err(e),
        }
    }

    pub fn normalize(self) -> Result<Configuration, ConfigurationError> {
        let current_dir = env::current_dir().map_err(ConfigurationError::Path)?;

        Ok(Configuration {
            input: self.input.map(|input| current_dir.join(input)),
            reference: current_dir.join(self.reference),
            description: current_dir.join(self.description),
            out_dir: current_dir.join(self.out_dir),
            haploblock: self.haploblock.map(|haploblock| HaploblockConfiguration {
                sequence: current_dir.join(haploblock.sequence),
                ..haploblock
            }),
            ..self
        })
    }

    pub fn validate(&self) -> Result<(), ConfigurationError> {
        if let Some(ref input) = self.input {
            if !input.is_file() {
                return Err(ConfigurationError::FileNotFound {
                    field: "input".to_owned(),
                    path: input.clone(),
                });
            }
        }

        if !self.reference.is_file() {
            return Err(ConfigurationError::FileNotFound {
                field: "reference".to_owned(),
                path: self.reference.clone(),
            });
        }

        if !self.description.is_file() {
            return Err(ConfigurationError::FileNotFound {
                field: "description".to_owned(),
                path: self.description.clone(),
            });
        }

        if self.shift == 0 {
            return Err(ConfigurationError::ZeroValue {
                field: "shift".to_owned(),
            });
        }

        if self.size == 0 {
            return Err(ConfigurationError::ZeroValue {
                field: "size".to_owned(),
            });
        }

        if let Some(ref haploblock) = self.haploblock {
            if !haploblock.sequence.is_file() {
                return Err(ConfigurationError::FileNotFound {
                    field: "haploblock.sequence".to_owned(),
                    path: haploblock.sequence.clone(),
                });
            }
        }

        Ok(())
    }
}
