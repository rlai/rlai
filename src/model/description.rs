use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::path::Path;

use csv;
use serde_derive::{Deserialize, Serialize};

use crate::error::Configuration as ConfigurationError;
use crate::model::ancestry::{Population, PopulationHash};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Entry {
    #[serde(rename = "Sample name")]
    pub sample_id: String,
    #[serde(rename = "Population code")]
    pub pop_code: String,
    #[serde(rename = "Population name")]
    pub pop_id: String,
}

#[derive(Clone, Debug)]
pub struct Description {
    sample_ids: Vec<String>,
    populations: Vec<Population>,
    pop_idx_by_pop_hash: HashMap<PopulationHash, usize>,
    pop_hash_by_sample_id: HashMap<String, PopulationHash>,
}

fn compute_hash<H: Hash>(value: &H) -> u64 {
    let mut hasher = DefaultHasher::new();
    value.hash(&mut hasher);
    hasher.finish()
}

fn wrap_in_custom(csv_error: csv::Error) -> ConfigurationError {
    ConfigurationError::Tsv {
        field: "description".to_owned(),
        cause: csv_error,
    }
}

impl Description {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Description, ConfigurationError> {
        let mut rdr = csv::ReaderBuilder::new()
            .delimiter(b'\t')
            .from_path(path)
            .map_err(wrap_in_custom)?;

        let entries = rdr
            .deserialize()
            .collect::<Result<Vec<Entry>, csv::Error>>()
            .map_err(wrap_in_custom)?;

        let pop_by_sample_id: HashMap<String, Population> = entries
            .into_iter()
            .map(|e| {
                (
                    e.sample_id,
                    Population {
                        id: e.pop_id,
                        code: e.pop_code,
                    },
                )
            }).collect();

        let sample_ids = pop_by_sample_id.keys().cloned().collect();

        let mut populations: Vec<Population> = pop_by_sample_id.values().cloned().collect();

        populations.sort_unstable();
        populations.dedup();

        let pop_idx_by_pop_hash: HashMap<PopulationHash, usize> = populations
            .iter()
            .enumerate()
            .map(|(i, pop)| (compute_hash(pop), i))
            .collect();

        let pop_hash_by_sample_id: HashMap<String, PopulationHash> = pop_by_sample_id
            .into_iter()
            .map(|(sample_name, pop)| (sample_name, compute_hash(&pop)))
            .collect();

        Ok(Description {
            sample_ids,
            populations,
            pop_idx_by_pop_hash,
            pop_hash_by_sample_id,
        })
    }

    pub fn hash<T: AsRef<str>>(&self, sample_id: T) -> PopulationHash {
        self.pop_hash_by_sample_id[sample_id.as_ref()]
    }

    pub fn population(&self, pop_hash: PopulationHash) -> &Population {
        let pop_idx = self.pop_idx_by_pop_hash[&pop_hash];
        &self.populations[pop_idx]
    }

    pub fn sample_ids(&self) -> &[String] {
        &self.sample_ids
    }

    pub fn populations(&self) -> &[Population] {
        &self.populations
    }
}
