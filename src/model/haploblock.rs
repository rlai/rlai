use std::path::Path;

use csv;
use serde_derive::{Deserialize, Serialize};

use crate::error::Configuration as ConfigurationError;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Haploblock {
    #[serde(rename = "chr")]
    pub chr_id: String,
    #[serde(rename = "start")]
    pub left_bound: usize,
    #[serde(rename = "stop")]
    pub right_bound: usize,
}

pub struct HaploblockSequence(pub Vec<Haploblock>);

fn wrap_in_custom(csv_error: csv::Error) -> ConfigurationError {
    ConfigurationError::Tsv {
        field: "haploblock_seq".to_owned(),
        cause: csv_error,
    }
}

impl HaploblockSequence {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<HaploblockSequence, ConfigurationError> {
        let mut rdr = csv::ReaderBuilder::new()
            .delimiter(b'\t')
            .from_path(path)
            .map_err(wrap_in_custom)?;

        let haploblocks = rdr
            .deserialize()
            .collect::<Result<Vec<Haploblock>, csv::Error>>()
            .map_err(wrap_in_custom)?;

        Ok(HaploblockSequence(haploblocks))
    }
}

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum WeighingMethod {
    #[serde(rename = "harmonic")]
    Harmonic,
    #[serde(rename = "mismatch_count")]
    MismatchCount,
}
