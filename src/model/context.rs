use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

use log::{debug, info, log};

use crate::error;
use crate::htslib;
use crate::model::configuration::Configuration;
use crate::model::description::Description;
use crate::model::genotype::{GenotypeSet, Schema};
use crate::model::haploblock::{HaploblockSequence, WeighingMethod};

pub struct HaploblockContext {
    pub sequence: HaploblockSequence,
    pub weighing_method: WeighingMethod,
}

pub struct Reference {
    pub ids: Vec<String>,
    pub schema: Schema,
    pub genotype_set: GenotypeSet,
    pub description: Description,
    pub haploblock_ctx: Option<HaploblockContext>,
}

pub struct Inference {
    pub ids: Vec<String>,
    pub schema: Schema,
    pub genotype_set: GenotypeSet,
    pub reference: Reference,
}

pub enum Context {
    Inference(Inference),
    Reference(Reference),
}

fn indexing_according_to_superseq(xs: &[String], superseq: &[String]) -> Vec<usize> {
    let idx_from_x: HashMap<&String, usize> = xs.iter().enumerate().map(|(i, x)| (x, i)).collect();

    let mut reindex = vec![0; xs.len()];

    for (i, y) in superseq.iter().enumerate() {
        if let Some(&idx) = idx_from_x.get(y) {
            reindex[idx] = i;
        }
    }

    reindex
}

fn index_according_to_superseq(
    sample_ids: &[String],
    available_ids: &[String],
) -> Result<Vec<(usize, String)>, error::Configuration> {
    let set: HashSet<&String> = HashSet::from_iter(sample_ids.iter());
    let available_set: HashSet<&String> = HashSet::from_iter(available_ids.iter());

    if !set.is_subset(&available_set) {
        return Err(error::Configuration::UnknownSampleId);
    }

    let indexing = indexing_according_to_superseq(&sample_ids, &available_ids);

    Ok(indexing
        .into_iter()
        .zip(sample_ids.iter().cloned())
        .collect())
}

impl Context {
    pub fn new(configuration: &Configuration) -> Result<Context, error::Execution> {
        let available_ids =
            htslib::load_sample_ids(&configuration.reference).map_err(error::Execution::Input)?;

        debug!("Read sample ids from reference file.");

        let haploblock_ctx = if let Some(ref haploblock) = configuration.haploblock {
            let haploblock_seq = HaploblockSequence::from_path(&haploblock.sequence)
                .map_err(error::Execution::Configuration)?;

            info!("Haploblock sequence was successfully read.");

            Some(HaploblockContext {
                sequence: haploblock_seq,
                weighing_method: haploblock.weighing_method,
            })
        } else {
            None
        };

        let description = Description::from_path(&configuration.description)
            .map_err(error::Execution::Configuration)?;

        debug!("Read description file.");

        let described_ids = description.sample_ids().to_owned();

        let indexed_reference_ids = index_according_to_superseq(&described_ids, &available_ids)
            .map_err(error::Execution::Configuration)?;

        info!("All described sample ids are present in reference file.");

        let (reference_schema, reference_set) =
            htslib::load_genotype_set_with_schema(&configuration.reference, &indexed_reference_ids)
                .map_err(error::Execution::Input)?;

        info!("Reference data was successfully read.");

        let reference = Reference {
            ids: described_ids,
            schema: reference_schema,
            genotype_set: reference_set,
            description,
            haploblock_ctx,
        };

        if let Some(ref input) = configuration.input {
            let input_ids = htslib::load_sample_ids(input).map_err(error::Execution::Input)?;

            debug!("Read sample ids from input file.");

            let indexed_input_ids = input_ids
                .iter()
                .cloned()
                .enumerate()
                .collect::<Vec<(usize, String)>>();

            let (input_schema, input_set) =
                htslib::load_genotype_set_with_schema(input, &indexed_input_ids)
                    .map_err(error::Execution::Input)?;

            info!("Input data was successfully read.");

            if input_schema != reference.schema {
                return Err(error::Execution::Input(error::Input::SchemaMismatch));
            }

            Ok(Context::Inference(Inference {
                ids: input_ids,
                schema: input_schema,
                genotype_set: input_set,
                reference,
            }))
        } else {
            Ok(Context::Reference(reference))
        }
    }
}
