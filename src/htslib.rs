use std::collections::BTreeMap;
use std::path::{Path, PathBuf};

use csv;
use log::{info, log};
use rust_htslib::bcf;
use rust_htslib::bcf::record::GenotypeAllele::{Phased, Unphased};
use rust_htslib::bcf::Read;

use crate::error::Input as InputError;
use crate::model::genotype::{Genotype, GenotypeSet, Schema};
use crate::progress::ProgressBuilder;

/// Extracts allele indices from a genotype record.
///
/// `htslib` provides genotypes as arrays of allele indices.
/// These indices are tagged as phased/unphased.
///
/// Note that the result complies with the BCF spec. This means that the
/// first allele will always be marked as `Unphased`. That is, if you have 1|1 in the VCF,
/// this method will return `[Unphased(1), Phased(1)]`.
///
/// # Errors
/// This function will return an error if any genotypes are missing or unphased.
fn allele_indices_from_genotype(
    sample_id: &str,
    marker_pos: u32,
    genotype: &bcf::record::Genotype,
) -> Result<(u8, u8), InputError> {
    match (&genotype[0], &genotype[1]) {
        (&Unphased(idx_lt), &Phased(idx_rt)) => Ok((idx_lt as u8, idx_rt as u8)),
        (&Unphased(_), &Unphased(_)) => Err(InputError::UnphasedGenotype {
            sample_id: sample_id.to_owned(),
            marker_pos,
        }),
        _ => Err(InputError::MissingGenotype {
            sample_id: sample_id.to_owned(),
            marker_pos,
        }),
    }
}

pub fn load_sample_ids<P: AsRef<Path>>(path: P) -> Result<Vec<String>, InputError> {
    let bcf_rdr = bcf::Reader::from_path(path).map_err(InputError::Path)?;

    Ok(bcf_rdr
        .header()
        .samples()
        .into_iter()
        .map(|id_bytes| String::from_utf8_lossy(id_bytes).into_owned())
        .collect())
}

fn load_nb_records<P: AsRef<Path>>(path: P) -> Option<usize> {
    csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(&path)
        .ok()
        .and_then(|mut rdr| rdr.records().next())
        .and_then(|read_result| read_result.ok())
        .and_then(|rcd| rcd.get(0).and_then(|val| val.parse::<usize>().ok()))
}

/// Create a collection of sample_ids and corresponding genotypes from the given file path.
///
/// This function reads all individuals from the BCF input and collects all markers.
///
/// # Errors
/// This function will return an error on any failure to read the BCF input and
/// if there are any unphased/missing genotypes.
pub fn load_genotype_set_with_schema<P>(
    path: P,
    indexed_sample_ids: &[(usize, String)],
) -> Result<(Schema, GenotypeSet), InputError>
where
    P: AsRef<Path>,
{
    let mut bcf_rdr = bcf::Reader::from_path(&path).map_err(InputError::Path)?;

    let nb_records_path = PathBuf::from(format!("{}.{}", path.as_ref().to_string_lossy(), "count"));
    let nb_records = if let Some(nb_records) = load_nb_records(&nb_records_path) {
        nb_records
    } else {
        info!(
            "Specify number of records in {:?} for a valid progressbar.",
            nb_records_path
        );
        0
    };

    let mut schema: Schema = Vec::with_capacity(nb_records);

    let mut genotype_set: BTreeMap<String, Genotype> = indexed_sample_ids
        .iter()
        .map(|&(_, ref sample_id)| {
            (
                sample_id.to_owned(),
                Genotype(
                    Vec::with_capacity(nb_records),
                    Vec::with_capacity(nb_records),
                ),
            )
        }).collect();

    let progress_bar = ProgressBuilder::new()
        .start_message("Reading data from disk...")
        .size(nb_records)
        .finish_message("Finished reading data from disk.")
        .build();

    progress_bar
        .stream(bcf_rdr.records(), |mb_record| {
            let mut record = mb_record.map_err(InputError::Read)?;

            let record_pos = record.pos();

            schema.push(record_pos);

            let record_genotypes = record.genotypes().map_err(InputError::Format)?;

            for &(idx, ref sample_id) in indexed_sample_ids {
                let genotype = record_genotypes.get(idx);

                let (idx_lt, idx_rt) =
                    allele_indices_from_genotype(sample_id, record_pos, &genotype)?;

                let sample_genotype = genotype_set.get_mut(sample_id).unwrap();

                sample_genotype.0.push(idx_lt);
                sample_genotype.1.push(idx_rt);
            }

            Ok(())
        }).collect::<Result<Vec<()>, InputError>>()?;

    Ok((schema, genotype_set))
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::BTreeSet;
    use std::path::PathBuf;

    fn indexed_sample_ids() -> Vec<(usize, String)> {
        let sample_ids = vec![
            String::from("NA00001"),
            String::from("NA00002"),
            String::from("NA00003"),
        ];
        sample_ids.into_iter().enumerate().collect()
    }

    #[test]
    fn test_valid_path() {
        let valid_path = PathBuf::from("examples/data/spec-v4.3.bcf");
        assert!(
            load_genotype_set_with_schema(&valid_path, &indexed_sample_ids()).is_ok(),
            "valid file wasn't read"
        );
    }

    #[test]
    fn test_invalid_path() {
        let invalid_path = PathBuf::from("examples/data/invalid_path.bcf");
        assert!(
            load_genotype_set_with_schema(&invalid_path, &indexed_sample_ids()).is_err(),
            "non-existing file was read"
        );
    }

    #[test]
    fn test_missing_fails() {
        let missing_path = PathBuf::from("examples/data/spec-v4.3-missing.bcf");
        let maybe_genotypes = load_genotype_set_with_schema(&missing_path, &indexed_sample_ids());
        assert!(
            maybe_genotypes.is_err(),
            "file with missing genotypes was read"
        );

        if let Err(InputError::MissingGenotype {
            sample_id,
            marker_pos,
        }) = maybe_genotypes
        {
            assert_eq!(
                "NA00001", sample_id,
                "file with missing genotypes returned incorrect sample name"
            );
            assert_eq!(
                17329, marker_pos,
                "file with missing genotypes returned incorrect position"
            );
        } else {
            assert!(
                false,
                "file with missing genotypes returned incorrect error"
            )
        }
    }

    fn read_example_genotypes() -> GenotypeSet {
        let valid_path = PathBuf::from("examples/data/spec-v4.3.bcf");
        let (_, genotype_set) = load_genotype_set_with_schema(&valid_path, &indexed_sample_ids())
            .expect("example file exists and is valid");

        genotype_set
    }

    #[test]
    fn test_number_of_samples() {
        let genotype_set = read_example_genotypes();

        assert_eq!(3, genotype_set.len());
    }

    #[test]
    fn test_chromosome_length() {
        let genotype_set = read_example_genotypes();

        let set_of_chr_lengths: BTreeSet<_> = genotype_set
            .values()
            .map(|g| [g.0.len(), g.1.len()])
            .collect();

        assert_eq!(
            1,
            set_of_chr_lengths.len(),
            "at least on of the chromosomes has a different length"
        );
    }

    // fn owned_bytes_from_str(s: &str) -> Vec<u8> {
    //     s.as_bytes().to_owned()
    // }

    // fn zip_and_expand_genotypes(
    //     gt_set: GenotypeSet,
    //     schema: Schema,
    // ) -> BTreeMap<String, Vec<(Allele, Allele)>> {
    //     gt_set
    //         .iter()
    //         .map(|(sample, gt)| {
    //             let lt: Vec<Allele> = gt
    //                 .0
    //                 .iter()
    //                 .zip(schema)
    //                 .map(|(&al_idx, als)| als[al_idx as usize].clone())
    //                 .collect();

    //             let rt: Vec<Allele> = gt
    //                 .1
    //                 .iter()
    //                 .zip(schema)
    //                 .map(|(&al_idx, als)| als[al_idx as usize].clone())
    //                 .collect();

    //             (sample.clone(), lt.into_iter().zip(rt.into_iter()).collect())
    //         })
    //         .collect()
    // }

    // #[test]
    // fn test_read_values() {
    //     let (schema, genotype_set) = read_example_genotypes();

    //     let mut expected_values: BTreeMap<String, Vec<(Allele, Allele)>> = BTreeMap::new();
    //     expected_values.insert(
    //         "NA00001".to_string(),
    //         vec![
    //             (owned_bytes_from_str("G"), owned_bytes_from_str("G")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("G"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("GTC"), owned_bytes_from_str("G")),
    //         ],
    //     );
    //     expected_values.insert(
    //         "NA00002".to_string(),
    //         vec![
    //             (owned_bytes_from_str("A"), owned_bytes_from_str("G")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("A")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("G")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("GTC"), owned_bytes_from_str("GTCT")),
    //         ],
    //     );
    //     expected_values.insert(
    //         "NA00003".to_string(),
    //         vec![
    //             (owned_bytes_from_str("A"), owned_bytes_from_str("A")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("T"), owned_bytes_from_str("T")),
    //             (owned_bytes_from_str("G"), owned_bytes_from_str("G")),
    //         ],
    //     );

    //     assert_eq!(
    //         expected_values,
    //         zip_and_expand_genotypes(genotype_set, schema)
    //     );
    // }
}
