use std::fmt::Display;

use indicatif::{ProgressBar, ProgressStyle};

#[derive(Clone)]
pub struct Progress {
    start_message: String,
    format_message: Option<String>,
    finish_message: String,
    size: Option<usize>,
}

impl Progress {
    pub fn traverse<T, I, F, U>(&self, iter: I, mut f: F) -> impl Iterator<Item = U>
    where
        T: Display,
        I: ExactSizeIterator<Item = T>,
        F: FnMut(T) -> U,
    {
        let progress_bar = ProgressBar::new(iter.len() as u64);

        progress_bar.set_style(
            ProgressStyle::default_bar()
                .template(
                    "[{elapsed_precise} | {eta}] [{progress_bar:40.cyan/blue}] [{pos}/{len} | {percent}%] {msg}",
                )
                .progress_chars("#>-"),
        );

        progress_bar.set_message(&self.start_message);

        let values = progress_bar
            .wrap_iter(iter)
            .map(|value| {
                if let Some(ref template) = self.format_message {
                    let str_value = format!("{}", &value);
                    progress_bar.set_message(&template.replace("{}", &str_value));
                }

                f(value)
            }).collect::<Vec<_>>();

        progress_bar.finish_with_message(&self.finish_message);

        values.into_iter()
    }

    pub fn stream<T, I, F, U>(&self, iter: I, f: F) -> impl Iterator<Item = U>
    where
        I: Iterator<Item = T>,
        F: FnMut(T) -> U,
    {
        let progress_bar = ProgressBar::new(self.size.unwrap_or(0) as u64);

        progress_bar.set_style(
            ProgressStyle::default_bar()
                .template(
                    "[{elapsed_precise} | {eta}] [{progress_bar:40.cyan/blue}] [{pos}/{len} | {percent}%] {msg}",
                )
                .progress_chars("#>-"),
        );

        progress_bar.set_message(&self.start_message);

        let values = progress_bar.wrap_iter(iter).map(f).collect::<Vec<_>>();

        progress_bar.finish_with_message(&self.finish_message);

        values.into_iter()
    }
}

pub struct ProgressBuilder {
    progress: Progress,
}

impl ProgressBuilder {
    pub fn new() -> ProgressBuilder {
        let progress = Progress {
            start_message: "Loop started.".to_string(),
            format_message: None,
            finish_message: "Loop finished.".to_string(),
            size: None,
        };
        ProgressBuilder { progress }
    }

    pub fn build(&self) -> Progress {
        self.progress.clone()
    }

    pub fn start_message(&mut self, message: &str) -> &mut ProgressBuilder {
        self.progress.start_message = message.to_string();
        self
    }

    pub fn format_message(&mut self, message: &str) -> &mut ProgressBuilder {
        self.progress.format_message = Some(message.to_string());
        self
    }

    pub fn finish_message(&mut self, message: &str) -> &mut ProgressBuilder {
        self.progress.finish_message = message.to_string();
        self
    }

    pub fn size(&mut self, size: usize) -> &mut ProgressBuilder {
        self.progress.size = Some(size);
        self
    }
}

impl Default for ProgressBuilder {
    fn default() -> Self {
        Self::new()
    }
}
