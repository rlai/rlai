<h1 align="center">
  <br>
  <a href="https://gitlab.com/rlai/rlai"><img src="https://gitlab.com/rlai/rlai/raw/master/rlai.png" alt="rLAI" width="100"></a>
  <br>
  <a>rLAI</a>
  <br>
</h1>

Ancestry made easy.

[![build status](https://gitlab.com/rlai/rlai/badges/master/build.svg)](https://gitlab.com/rlai/rlai/commits/master)

```json
{
	"input": "/Users/idmit/Documents/dev/sbox/rlai_product/examples/data/spec-v4.3.bcf",
	"reference": "/Users/idmit/Documents/dev/sbox/rlai_product/examples/data/spec-v4.3.bcf",
	"description": "/Users/idmit/Documents/dev/exomanc/data/igsr/igsr_description.tsv",
	"shift": 1,
	"size": 500
}
```